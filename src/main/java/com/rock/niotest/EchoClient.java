package com.rock.niotest;

import java.io.InputStream;
import java.net.Socket;

public class EchoClient {

    public static void main(String[] args) throws Exception{
       
        Socket socket = new Socket("localhost", 8888);
        socket.getOutputStream().write("hello this is rock".getBytes());
        socket.getOutputStream().flush();
        
        byte[] buf = new byte[1024];
        InputStream in = socket.getInputStream();
        int len = in.read(buf);
        System.out.println("from server:" + new String(buf, 0, len));
        
        socket.close();
        System.out.println("server closed");
    }

}
